package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Member: Mattia Bertorello
 * Date: 23/03/13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Post extends Model {

    public static Model.Finder<Long, Post> find = new Model.Finder(Long.class, Post.class);
    @Id
    public Long id;
    @Constraints.Required
    public String text;
    public Date createdOn;
    @OneToMany(cascade = CascadeType.REMOVE)
    public List<Comment> comments = new ArrayList<Comment>();

    @ManyToOne
    public Member member;

    public static List<Post> fromCreatedOn(Date from) {
        return find.where().gt("createdOn", from).findList();
    }

    public static List<Post> page(Integer page) {
        return find.where().orderBy("createdOn desc")
                .findPagingList(10)
                .getPage(page).getList();
    }

    public static void create(Post post) {
        post.save();
    }

    public void save() {
        this.createdOn = new Date();
        super.save();
    }

}
