package models;

import org.codehaus.jackson.annotate.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Member: Mattia Bertorello
 * Date: 23/03/13
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Member extends Model {
    public static Finder<String, Member> find = new Finder<String, Member>(
            String.class, Member.class
    );
    @Id
    public String email;
    public String name;
    @Constraints.Required
    @JsonIgnore
    public String password;

    public Date createdOn;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JsonIgnore
    public List<Comment> comments = new ArrayList<Comment>();

    @OneToMany(cascade = CascadeType.REMOVE)
    @JsonIgnore
    public List<Post> posts = new ArrayList<Post>();

    public Member() {
    }

    public Member(String email, String name, String password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public static Member authenticate(String email, String password) {
        return find.where().eq("email", email)
                .eq("password", password).findUnique();
    }

    public static void create(Member member) {
        member.save();
    }

    public void save() {
        this.createdOn = new Date();
        super.save();
    }
}
