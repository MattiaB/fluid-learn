package models;


import org.codehaus.jackson.annotate.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Member: Mattia Bertorello
 * Date: 23/03/13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Comment extends Model {
    public static Model.Finder<Long, Comment> find = new Model.Finder(Long.class, Comment.class);
    @Id
    public Long id;
    @Constraints.Required
    public String text;
    public Date createdOn;
    @ManyToOne
    @JsonIgnore
    public Post post;
    @ManyToOne
    public Member member;

    public static void create(Comment comment) {
        comment.save();
    }

    public void save() {
        this.createdOn = new Date();
        super.save();
    }


}
