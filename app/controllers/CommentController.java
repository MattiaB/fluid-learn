package controllers;

import models.Comment;
import models.Member;
import models.Post;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

import static play.data.Form.form;

/**
 * Created with IntelliJ IDEA.
 * Member: Mattia Bertorello
 * Date: 23/03/13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
public class CommentController extends Controller {
    public static Result all() {
        List<Comment> commentmList = Comment.find.all();
        JsonNode jsonNode = Json.toJson(commentmList);
        return ok(jsonNode);
    }

    public static Result show() {
        return ok("");
    }

    @Security.Authenticated(Secured.class)
    public static Result create(Long postId) {
        Comment newComment = null;
        Post commentPost = null;
        Form<Comment> commentForm = form(Comment.class).bindFromRequest();
        if (commentForm.hasErrors()) {
            return badRequest("");
        } else {
            commentPost = Post.find.byId(postId);
            newComment = commentForm.get();
            newComment.member = Member.find.byId(session("email"));
            newComment.post = commentPost;
            Comment.create(newComment);
        }
        JsonNode jsonNode = Json.toJson(newComment);
        ObjectNode objectNode = Json.newObject();
        objectNode.put("post_id", commentPost.id);
        return ok(objectNode.putAll((ObjectNode) jsonNode));
    }
}
