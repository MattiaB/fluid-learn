package controllers;

import models.Member;
import play.cache.Cached;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.login;
import views.html.singup;

import static play.data.Form.form;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render());
    }

    @Cached(key = "login", duration = 86400)
    public static Result login() {
        return ok(
                login.render(form(Login.class))
        );
    }

    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(
                routes.Application.index()
        );
    }

    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            session().clear();
            Member member = Member.find.byId(loginForm.get().email);
            session("email", member.email);
            session("name", member.name);
            return redirect(
                    routes.Application.index()
            );
        }
    }

    @Cached(key = "singup", duration = 86400)
    public static Result singup() {
        return ok(
                singup.render(form(Member.class))
        );
    }

    public static Result createUser() {
        Form<Member> userForm = form(Member.class).bindFromRequest();
        if (userForm.hasErrors()) {
            return badRequest(singup.render(userForm));
        } else {
            session().clear();
            session("email", userForm.get().email);
            session("name", userForm.get().name);

            Member.create(userForm.get());
        }

        return redirect(
                routes.Application.index()
        );
    }

    public static class Login {

        public String email;
        public String password;

        public String validate() {
            if (Member.authenticate(email, password) == null) {
                return "Invalid member or password";
            }
            return null;
        }
    }


}
