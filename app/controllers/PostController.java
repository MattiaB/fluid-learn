package controllers;

import models.Member;
import models.Post;
import org.codehaus.jackson.JsonNode;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.LinkedList;
import java.util.List;

import static play.data.Form.form;

/**
 * Created with IntelliJ IDEA.
 * Member: Mattia Bertorello
 * Date: 20/03/13
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */
public class PostController extends Controller {

    static Form<Post> taskForm = Form.form(Post.class);


    public static Result all(Long from) {
        List<Post> postList = new LinkedList<Post>();
        try {
            postList = Post.find.all();
        }  catch (Exception e) {

        }
        //if(from == 0){
        //    postList = Post.find.all();
        //}else{
        //    postList = Post.fromCreatedOn(new Date(from));
        //}
        JsonNode jsonNode = Json.toJson(postList);
        return ok(jsonNode);
    }

    public static Result show() {
        return ok("");
    }

    public static Result deleteAll() {
        List<Post> all = Post.find.all();
        for (Post post : all) {
            post.delete();
        }
        return ok("");
    }

    @Security.Authenticated(Secured.class)
    public static Result create() {
        Post newPost = null;
        Form<Post> postForm = form(Post.class).bindFromRequest();
        if (postForm.hasErrors()) {
            return badRequest(postForm.errorsAsJson());
        } else {
            Member member = Member.find.byId(session("email"));
            newPost = postForm.get();
            newPost.member = member;
            Post.create(newPost);
        }
        JsonNode jsonNode = Json.toJson(newPost);
        return ok(jsonNode);
    }
}
